# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime
from web3 import Web3, HTTPProvider, IPCProvider, WebsocketProvider
from web3.contract import ConciseContract
from solc import compile_standard
from odoo.exceptions import ValidationError
from PIL import Image, ImageDraw, ImageFont
import pandas as pd
from random import SystemRandom
import base64
import re
import json
import sys

class Properties(models.Model):
    _name = 'tokenizacion.properties'

    name = fields.Char(string="Nombre del desarrollo", size=24 )
    street = fields.Char(string="Dirección del inmueble")
    street2 = fields.Char(string="Calle")
    zip = fields.Char(string="Código postal")
    city = fields.Char(string="Ciudad")
    country_id = fields.Many2one('res.country', help='Selecciona un país', string="País", ondelete='restrict')
    state_id = fields.Many2one('res.country.state', help='Selecciona un estado', string="País", ondelete='restrict')
    noInt = fields.Char(string="No. Interior")
    noExt = fields.Char(string="No. Exterior")
    municipality = fields.Char(string="Municipio")
    address = fields.Char(string="Municipio")
    neighborhood = fields.Char(string="Colonia")
    telephone = fields.Char()
    email = fields.Char(string="Correo electrónico")
    total_property = fields.Char(string="Valor total del inmueble(USD)")
    investment_type = fields.Char(default="Inmueble")
    images = fields.Many2many('ir.attachment', string="Imagen")
    files = fields.Binary(string="Subir archivo")
    creation_date = fields.Date(string="Fecha de consulta", default=datetime.today())
    percentage = fields.Selection(selection=lambda self: self.dynamic_selection(), string="Porcentaje del inmueble para inversión colaborativa")
    typeProperty = fields.Many2one('tokenizacion.type.property', string="Tipo de propiedad")
    m2Property = fields.Char(string="m² de propiedad")
    m2Construction = fields.Char(string="m² de construcción")
    imageTop = fields.Binary()
    faq_ids = fields.One2many('tokenizacion.faq', 'inmuebles_ids', string="FAQ")
    date_con = fields.Date(string="Fecha de cierre")
    # hash_contract = fields.Char()
    hash_contract_payment = fields.Char()
    hash_contract_investment = fields.Char()
    price_token = fields.Integer(string="Precio por token")
    imgtest = fields.Binary()
    per_ter = fields.Selection(selection=lambda self: self.dynamic_selection(), string="Porcentaje de rendimiento")
    p_yield = fields.Selection(selection=lambda self: self.dynamic_selection_dates(), string="Plazo de rendimiento")
    flag_modify = fields.Boolean(default=False)
    # check_computed_total_property = fields.Char(string="flag compute", compute='_modify_total_value')
    # check_flag_computed_total = fields.Char(string="flag compute", compute='_modify_total_value')
    # google_map_partner = fields.Char(string="Map")
    status = fields.Selection([
            ('public', 'Publicado'),
            ('sale', 'En venta'),
            ('final', 'Concluído'),
            ],default='public')

    #Funcion para estatus activo
    # @api.model
    def public_progressbar(self):
        self.write({
        'status': 'public',
    })
 
    #Funcion para estatus inactivo
    # @api.model
    def sale_progressbar(self):
        self.write({
        'status': 'sale',
    })

     #Funcion para estatus inactivo
    # @api.model
    def final_progressbar(self):
        self.write({
        'status': 'final',
    })
    
    #Funcion para selección de procentajes
    # @api.model
    def dynamic_selection(self):

      select = [('10', '10%'), ('20', '20%'), ('30', '30%'), ('40', '40%'), ('50', '50%'), ('60', '60%'), ('70', '70%'), ('80', '80%'), ('90', '90%'), ('100', '100%')]

      return select

    #Funcion para selección de plazos de rendimiento
    def dynamic_selection_dates(self):

      select = [('90', '3 meses'), ('180', '6 meses'), ('365', '1 año'), ('730', '2 años'), ('1095', '3 años'), ('120', '60 seg prueba eliminar')]

      return select

    #email validation
    @api.onchange('email')
    def validate_mail(self):
       if self.email:
        match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', self.email)
        if match == None:
            raise ValidationError('No es un e-mail válido')
    
    
    # Create in properties models!!!!
    @api.model
    def create(self, vals):
        
        print ("entro a la sobrecarga create de properties")
        print("Valores create", vals)
        
        # -------------------------------------------------------------
        # Testing V3 -----------------------------------------------------------------------
        w3 = Web3(HTTPProvider('https://ropsten.infura.io/v3/f9736385b2d340dd976ab1df6ff51ad6'))
        # Environment V3 -----------------------------------------------------------------------
        #w3 = Web3(HTTPProvider('https://mainnet.infura.io/v3/f9736385b2d340dd976ab1df6ff51ad6'))
        # --------------------------------step 2
        # if len(sys.argv) <= 1:
        #testing
        path = "/home/dpat/TokenInvestment.json"
        #environment
        #path = "/opt/odoo13/odoo-custom-addons/TokenInvestment.json"
        # save1 = super(Properties, self).create(vals)
        _name = vals['name']
        _percentage = int(vals['percentage'])
        records = self.env['tokenizacion.properties'].search([])
        last = records and max(records)
        last_id = last.id
        print("A ver los records", last_id)
        
        if last_id == False:
            _ids = 0
        else:
            _ids = self.env['tokenizacion.properties'].search([])[-1].id
        # _ids = self.env['tokenizacion.properties'].search([])[-1].id
        # _ids = self.env['tokenizacion.properties'].search([]).id
        # print("es _ids1",_ids1)
        # print("es _ids correct",_ids)
        # if _ids == False:
        #     _ids = 0
        # else:
        #     _ids = _ids
        print("es _ids",_ids)
        _id = _ids + 1 
        print ("Se supone que es el id",_id)
        def get_symbol(self,_name,_id):
            words = _name.split(' ') 
            symbol = ''
            for word in words:
                symbol += word[0]
            return symbol + str(_id)
        _symbol = get_symbol(self,_name,_id)
        print("qué symbol es", _symbol)
        print(f"USAGE: python3 {sys.argv[0]} <TokenInvestment.json>") 
        try:
            with open(path) as inFile:
                solcOutput = json.load(inFile)
        except Exception as e:
            print(f"ERROR: Could not load file {path}: {e}")
        senderKey = "32ED1791B32C9FEFE2DD3339276087A0AE6EE009E60D9C143591A445577A09F6"
        senderAccount = w3.eth.account.privateKeyToAccount(senderKey)
        contractData = solcOutput
        contractInvestment = w3.eth.contract(abi=contractData['abi'], bytecode=contractData['bin'])
        transactionInvestment = contractInvestment.constructor(_name, _symbol, _percentage).buildTransaction({
        'gasPrice': w3.toWei('31', 'gwei'),
        'from': senderAccount.address,
        'nonce': w3.eth.getTransactionCount(senderAccount.address)})
        # Sign the transaction
        signedInvestment = w3.eth.account.signTransaction(transactionInvestment, senderAccount.privateKey)
        # Send it!
        txHashInvestment = w3.eth.sendRawTransaction(signedInvestment.rawTransaction)
        print(f"Contract deployed for Investment; Waiting to transaction receipt")
        # Wait for the transaction to be mined, and get the transaction receipt
        txReceiptInvestment = w3.eth.waitForTransactionReceipt(txHashInvestment)
        print(f"Contract Investment deployed to: {txReceiptInvestment.contractAddress}")
        contractHashInvestment = txReceiptInvestment.contractAddress
        vals['hash_contract_investment'] = contractHashInvestment
        # DEPLOY FOR PAYMENT
        contractPayment = w3.eth.contract(abi=contractData['abi2'], bytecode=contractData['bin2'])
        transactionPayment = contractPayment.constructor(contractHashInvestment).buildTransaction({
        'gasPrice': w3.toWei('21', 'gwei'),
        'from': senderAccount.address,
        'nonce': w3.eth.getTransactionCount(senderAccount.address)})
        # Sign the transaction
        signedPayment = w3.eth.account.signTransaction(transactionPayment, senderAccount.privateKey)
        # Send it!
        txHashPayment = w3.eth.sendRawTransaction(signedPayment.rawTransaction)
        print(f"Contract deployed for Payment; Waiting to transaction receipt")
        # Wait for the transaction to be mined, and get the transaction receipt
        txReceiptPayment = w3.eth.waitForTransactionReceipt(txHashPayment)
        print(f"Contract Payment deployed to: {txReceiptPayment.contractAddress}")
        contractHashPayment = txReceiptPayment.contractAddress
        vals['hash_contract_payment'] = contractHashPayment
            
        res = super(Properties, self).create(vals)
        print(res)        

        return res


    
    # Write in properties models!!!!   
    #@api.model 
    def write(self,vals):

        print ("entro a la sobrecarga write de inmuebles")
        print ("Valores write: ",vals)
        
        # dateNow = datetime.now()
        # dateUpdate = self.write_date
        # days = int(self.p_yield)
        # percentage = int(self.per_ter)/100
        # print("date ahora",dateNow)
        # print("date inserted actual registered", dateUpdate)
        # print("days", days)

        # if dateUpdate == False:
        #     resDate = dateNow + timedelta(seconds=days)
        # else:
        #     resDate = dateUpdate + timedelta(seconds=days)
        
        # if dateNow >= resDate:
        #     print("La fecha está igual o mayor y se modificará el precio")
            
        #     record_ids = self.env['tokenizacion.properties'].search([('name', '=', self.name)])
        #     print("record_ids",record_ids)
        #     for record in record_ids:
        #         res = record.total_property
        #         print("res",res)
        #         toAdd = int(res) * percentage
        #         print("toAdd",toAdd)
        #         result = float(res) + toAdd
        #         print("result",result)
        #         finalRes = str(int(result))
        #         print("finalresult",finalRes)
        #         vals['total_property'] = finalRes
        # elif dateNow < resDate:
        #     print("La fecha aún no llega todavía a la seleccionada.")
        per_ter_database = self.per_ter
        print("flag in databse", per_ter_database)
        per_ter_vals = vals.get('per_ter')
        print("flag in vals", per_ter_vals)
        p_yield_database = self.p_yield
        print("flag in databse", p_yield_database)
        p_yield_vals = vals.get('p_yield')
        print("flag in vals", p_yield_vals)
        vals['flag_modify'] = True
        if self.per_ter != vals.get('per_ter') and vals.get('per_ter') != None:
            vals['flag_modify'] = False
        
        elif self.p_yield != vals.get('p_yield') and vals.get('p_yield') != None:
            vals['flag_modify'] = False


        res = super(Properties, self).write(vals)
        print(res)

        # checkF = self.modify_total_value()
        # print(checkF)

        
        return res

    
    def modify_total_value_properties(self):
        recordsall = self.env['tokenizacion.properties'].search([])
        for x in recordsall:
            l = x.id
            print(l)
            recordset = self.env['tokenizacion.properties'].search([('id', '=', l)])
            print("a ver si trae los ids normales", recordset)
            dateNow = datetime.now()
            dateUpdate = recordset.write_date
            days = int( recordset.p_yield)
            percentage = int(recordset.per_ter)/100
            print("date ahora",dateNow)
            print("date inserted actual registered", dateUpdate)
            print("days", days)

            # dateInit = dateNow + timedelta(seconds=0)
            # print("InitDate",dateInit)
            # dateFinish = dateNow - timedelta(seconds=60)
            # print("FinishDate",dateFinish)

            # recordDatesModif = self.env['tokenizacion.properties'].search([('write_date', '<', dateInit), ('write_date', '>', dateFinish)])
            # print("records modified between dates",recordDate)

            print("Si FALSE entra aql ciclo",recordset.flag_modify)
            

            if recordset.flag_modify == False:
                print("La fecha seleccionada aún no ha llegado así que queda pendiente cambiarse :).")

                if dateUpdate == None:
                    resDate = dateNow + timedelta(seconds=days)
                    print("resDate", resDate)
                else:
                    resDate = dateUpdate + timedelta(seconds=days)
                    print("resDate", resDate)
                
                if dateNow >= resDate:
                    print("La fecha está igual o mayor y se modificará el precio")
                    # record_ids = self.env['tokenizacion.properties'].search([('name', '=', self.name)])
                    # print("record_ids",record_ids)
                    # for record in record_ids:
                    res = recordset.total_property
                    print("res",res)
                    toAdd = int(res) * percentage
                    print("toAdd",toAdd)
                    result = float(res) + toAdd
                    print("result",result)
                    finalRes = str(int(result))
                    print("finalresult", finalRes)
                    vals1 = {
                    'total_property': finalRes,
                    'flag_modify': True
                    }
                    recordset.write(vals1)
                    print("cambio variable flag modify",recordset.flag_modify)

                else:
                    print("La fecha aún no llega todavía a la seleccionada.")
            
            else:
                print("La fecha ya ha sido seleccionada por lo que el monto ya ha sido cambiado :).")
        
        return True
             
        
        
        





class art(models.Model):
    _name = 'tokenizacion.art'

    name = fields.Char(string="Nombre de la obra de arte", size=24)
    num_serie = fields.Char(string="Número de serie")
    cert = fields.Char(string="No. Certificado")
    author = fields.Char(string="Artista")
    artWork_type = fields.Many2one('tokenizacion.type.art', string='Tipo de obra de arte')
    telephone = fields.Char()
    email = fields.Char(string="Correo electrónico")
    country = fields.Many2one('res.country', help='Selecciona un país', string="País", ondelete='restrict')
    total_artWork = fields.Char(string="Valor total de la obra(USD)")
    investment_type = fields.Char(default="Obra de arte")
    images = fields.Many2many('ir.attachment', string="Imagen")
    files = fields.Binary()
    creation_date = fields.Date(string="Fecha de consulta", default=datetime.today())
    percentage = fields.Selection(selection=lambda self: self.dynamic_selection(), string="Porcentaje de la obra para inversión colaborativa")
    imageTop = fields.Binary()
    faq_ids = fields.One2many('tokenizacion.faq', 'art_ids', string="FAQ")
    # hash_contract = fields.Char()
    hash_contract_payment = fields.Char()
    hash_contract_investment = fields.Char()
    dimention = fields.Char(string="Dimensiones de la obra de arte")
    price_token = fields.Integer(string="Precio por token")
    per_ter = fields.Selection(selection=lambda self: self.dynamic_selection(), string="Porcentaje de rendimiento")
    p_yield = fields.Selection(selection=lambda self: self.dynamic_selection_dates(), string="Plazo de rendimiento")
    flag_modify = fields.Boolean(default=False)
    status = fields.Selection([
            ('public', 'Publicado'),
            ('sale', 'En venta'),
            ('final', 'Concluído'),
            ],default='public')

     #Funcion para estatus activo
    # @api.model
    def public_progressbar(self):
        self.write({
        'status': 'public',
    })
 
    #Funcion para estatus inactivo
    # @api.model
    def sale_progressbar(self):
        self.write({
        'status': 'sale',
    })
    
    #Funcion para estatus inactivo
    # @api.model
    def final_progressbar(self):
        self.write({
        'status': 'final',
    })
    
    
    def dynamic_selection(self):

      select = [('10', '10%'), ('20', '20%'), ('30', '30%'), ('40', '40%'), ('50', '50%'), ('60', '60%'), ('70', '70%'), ('80', '80%'), ('90', '90%'), ('100', '100%')]

      return select

    
    #Funcion para selección de plazos de rendimiento
    def dynamic_selection_dates(self):

      select = [('90', '3 meses'), ('180', '6 meses'), ('365', '1 año'), ('730', '2 años'), ('1095', '3 años')]

      return select

    
    #email validation
    @api.onchange('email')
    def validate_mail(self):
       if self.email:
        match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', self.email)
        if match == None:
            raise ValidationError('No es un e-mail válido')

    # Create in art models!!!!
    @api.model
    def create(self, vals):
        
        print ("entro a la sobrecarga create de arts")
        print("Valores create art", vals)
        
        # -------------------------------------------------------------
        # Testing V3 -----------------------------------------------------------------------
        w3 = Web3(HTTPProvider('https://ropsten.infura.io/v3/f9736385b2d340dd976ab1df6ff51ad6'))
        # Environment V3 -----------------------------------------------------------------------
        #w3 = Web3(HTTPProvider('https://mainnet.infura.io/v3/f9736385b2d340dd976ab1df6ff51ad6'))
        # --------------------------------step 2
        # if len(sys.argv) <= 1:
        #testing
        path = "/home/dpat/TokenInvestment.json"
        #environment
        #path = "/opt/odoo13/odoo-custom-addons/TokenInvestment.json"
        # save1 = super(Properties, self).create(vals)
        _name = vals['name']
        _percentage = int(vals['percentage'])
        records = self.env['tokenizacion.art'].search([])
        last = records and max(records)
        last_id = last.id
        print("A ver los records", last_id)
        
        if last_id == False:
            _ids = 0
        else:
            _ids = self.env['tokenizacion.art'].search([])[-1].id
        # _ids = self.env['tokenizacion.properties'].search([])[-1].id
        # _ids = self.env['tokenizacion.properties'].search([]).id
        # print("es _ids1",_ids1)
        # print("es _ids correct",_ids)
        # if _ids == False:
        #     _ids = 0
        # else:
        #     _ids = _ids
        print("es _ids",_ids)
        # print("es _ids0",_ids0)
        # print("es _ids1",_ids1)
        _id = _ids + 1 
        print ("Se supone que es el id",_id)
        def get_symbol(self,_name,_id):
            words = _name.split(' ') 
            symbol = ''
            for word in words:
                symbol += word[0]
            return symbol + str(_id)
        _symbol = get_symbol(self,_name,_id)
        print("qué symbol es", _symbol)
        print(f"USAGE: python3 {sys.argv[0]} <TokenInvestment.json>") 
        try:
            with open(path) as inFile:
                solcOutput = json.load(inFile)
        except Exception as e:
            print(f"ERROR: Could not load file {path}: {e}")
        senderKey = "32ED1791B32C9FEFE2DD3339276087A0AE6EE009E60D9C143591A445577A09F6"
        senderAccount = w3.eth.account.privateKeyToAccount(senderKey)
        contractData = solcOutput
        contractInvestment = w3.eth.contract(abi=contractData['abi'], bytecode=contractData['bin'])
        transactionInvestment = contractInvestment.constructor(_name, _symbol, _percentage).buildTransaction({
        'gasPrice': w3.toWei('31', 'gwei'),
        'from': senderAccount.address,
        'nonce': w3.eth.getTransactionCount(senderAccount.address)})
        # Sign the transaction
        signedInvestment = w3.eth.account.signTransaction(transactionInvestment, senderAccount.privateKey)
        # Send it!
        txHashInvestment = w3.eth.sendRawTransaction(signedInvestment.rawTransaction)
        print(f"Contract deployed for Investment; Waiting to transaction receipt")
        # Wait for the transaction to be mined, and get the transaction receipt
        txReceiptInvestment = w3.eth.waitForTransactionReceipt(txHashInvestment)
        print(f"Contract Investment deployed to: {txReceiptInvestment.contractAddress}")
        contractHashInvestment = txReceiptInvestment.contractAddress
        vals['hash_contract_investment'] = contractHashInvestment
        # DEPLOY FOR PAYMENT
        contractPayment = w3.eth.contract(abi=contractData['abi2'], bytecode=contractData['bin2'])
        transactionPayment = contractPayment.constructor(contractHashInvestment).buildTransaction({
        'gasPrice': w3.toWei('21', 'gwei'),
        'from': senderAccount.address,
        'nonce': w3.eth.getTransactionCount(senderAccount.address)})
        # Sign the transaction
        signedPayment = w3.eth.account.signTransaction(transactionPayment, senderAccount.privateKey)
        # Send it!
        txHashPayment = w3.eth.sendRawTransaction(signedPayment.rawTransaction)
        print(f"Contract deployed for Payment; Waiting to transaction receipt")
        # Wait for the transaction to be mined, and get the transaction receipt
        txReceiptPayment = w3.eth.waitForTransactionReceipt(txHashPayment)
        print(f"Contract Payment deployed to: {txReceiptPayment.contractAddress}")
        contractHashPayment = txReceiptPayment.contractAddress
        vals['hash_contract_payment'] = contractHashPayment
            
        res = super(art, self).create(vals)


        return res
    
    
    # Write in art models!!!!
    #@api.model write 
    def write(self,vals):

        print ("entro a la sobrecarga write de arts")
        print ("Valores write: ",vals)
        
        

        # price = vals.get('price_token')
        # print("Precio token vals", price)
        # priceself = self.price_token
        # print("Precio token self", priceself)
        # if vals.get('price_token') != None:
        #     # Testing V3 -----------------------------------------------------------------------
        #     w3 = Web3(HTTPProvider('https://ropsten.infura.io/v3/f9736385b2d340dd976ab1df6ff51ad6'))
        #     # Environment V3 -----------------------------------------------------------------------
        #     #w3 = Web3(HTTPProvider('https://mainnet.infura.io/v3/f9736385b2d340dd976ab1df6ff51ad6'))
        #     _hash_contract = self.hash_contract
        #     print ("se supone que es el hash del contract",_hash_contract)
        #     token = w3.eth.contract(
        #     address = _hash_contract,
        #     abi = '[{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"spender","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":true,"internalType":"address","name":"to","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"constant":false,"inputs":[{"internalType":"string","name":"name","type":"string"},{"internalType":"string","name":"symbol","type":"string"},{"internalType":"uint8","name":"decimals","type":"uint8"},{"internalType":"uint256","name":"totalSupply","type":"uint256"},{"internalType":"uint256","name":"pricePerToken","type":"uint256"}],"name":"CreateTokenERC20","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"_owner","type":"address"},{"internalType":"address","name":"_spender","type":"address"}],"name":"allowance","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_spender","type":"address"},{"internalType":"uint256","name":"_value","type":"uint256"}],"name":"approve","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_spender","type":"address"},{"internalType":"uint256","name":"_value","type":"uint256"}],"name":"approveByOwner","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"balance","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"uint256","name":"_quantity","type":"uint256"}],"name":"buy","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"account","type":"address"},{"internalType":"uint256","name":"_quantity","type":"uint256"}],"name":"buyFor","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[{"internalType":"uint256","name":"pricePerToken","type":"uint256"}],"name":"changePricePerToken","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"internalType":"uint8","name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_spender","type":"address"},{"internalType":"uint256","name":"_subtractedValue","type":"uint256"}],"name":"decreaseApproval","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"internalType":"uint256","name":"quantity","type":"uint256"}],"name":"getAmountToPayed","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_spender","type":"address"},{"internalType":"uint256","name":"_addedValue","type":"uint256"}],"name":"increaseApproval","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_to","type":"address"},{"internalType":"uint256","name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_from","type":"address"},{"internalType":"address","name":"_to","type":"address"},{"internalType":"uint256","name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"}]'
        #     )
        #     transaction = token.functions.changePricePerToken(
        #     priceself ).buildTransaction({
        #     'gasPrice': w3.toWei('10', 'gwei'),
        #     #'gasPrice': w3.toWei(w3.eth.generateGasPrice()),
        #     'from': "0x7fB636cc460B8C7e4Aa3e6A22fE166EF5ec34eBB",
        #     'nonce': w3.eth.getTransactionCount("0x7fB636cc460B8C7e4Aa3e6A22fE166EF5ec34eBB")
        #     })
        #     private_key = "32ED1791B32C9FEFE2DD3339276087A0AE6EE009E60D9C143591A445577A09F6" 
        #     signed_txn = w3.eth.account.signTransaction(transaction, private_key=private_key)
        #     print("VER QUE HAY AQUI",signed_txn)
        #     finalwrite = w3.eth.sendRawTransaction(signed_txn.rawTransaction)
        #     final2write = finalwrite.hex()
        #     print("VER TAMBIEN QUE HAY AQUI write", final2write)
        
        # else: 
        #     print("Es igual el precio del token, así que nada")
        per_ter_database = self.per_ter
        print("flag in databse", per_ter_database)
        per_ter_vals = vals.get('per_ter')
        print("flag in vals", per_ter_vals)
        p_yield_database = self.p_yield
        print("flag in databse", p_yield_database)
        p_yield_vals = vals.get('p_yield')
        print("flag in vals", p_yield_vals)
        vals['flag_modify'] = True
        if self.per_ter != vals.get('per_ter') and vals.get('per_ter') != None:
            vals['flag_modify'] = False
        
        elif self.p_yield != vals.get('p_yield') and vals.get('p_yield') != None:
            vals['flag_modify'] = False

        
        res = super(art, self).write(vals)
        print(res)
        
        return res


    
    def modify_total_value_art(self):
        recordsall = self.env['tokenizacion.art'].search([])
        for x in recordsall:
            l = x.id
            print(l)
            recordset = self.env['tokenizacion.art'].search([('id', '=', l)])
            print("a ver si trae los ids normales", recordset)
            dateNow = datetime.now()
            dateUpdate = recordset.write_date
            days = int(recordset.p_yield)
            percentage = int(recordset.per_ter)/100
            print("date ahora",dateNow)
            print("date inserted actual registered", dateUpdate)
            print("days", days)

            # dateInit = dateNow + timedelta(seconds=0)
            # print("InitDate",dateInit)
            # dateFinish = dateNow - timedelta(seconds=60)
            # print("FinishDate",dateFinish)

            # recordDatesModif = self.env['tokenizacion.properties'].search([('write_date', '<', dateInit), ('write_date', '>', dateFinish)])
            # print("records modified between dates",recordDate)

            print("Si FALSE entra aql ciclo",recordset.flag_modify)
            

            if recordset.flag_modify == False:
                print("La fecha seleccionada aún no ha llegado así que queda pendiente cambiarse :).")

                if dateUpdate == None:
                    resDate = dateNow + timedelta(days=days)
                    print("resDate", resDate)
                else:
                    resDate = dateUpdate + timedelta(days=days)
                    print("resDate", resDate)
                
                if dateNow >= resDate:
                    print("La fecha está igual o mayor y se modificará el precio")
                    # record_ids = self.env['tokenizacion.properties'].search([('name', '=', self.name)])
                    # print("record_ids",record_ids)
                    # for record in record_ids:
                    res = recordset.total_artWork
                    print("res",res)
                    toAdd = int(res) * percentage
                    print("toAdd",toAdd)
                    result = float(res) + toAdd
                    print("result",result)
                    finalRes = str(int(result))
                    print("finalresult", finalRes)
                    vals1 = {
                    'total_property': finalRes,
                    'flag_modify': True
                    }
                    recordset.write(vals1)
                    print("cambio variable flag modify",recordset.flag_modify)

                else:
                    print("La fecha aún no llega todavía a la seleccionada.")
            
            else:
                print("La fecha ya ha sido seleccionada por lo que el monto ya ha sido cambiado :).")
        
        return True

    


class Projects(models.Model):
    _name = 'tokenizacion.projects'

    name = fields.Char(string="Nombre del proyecto", size=24)
    area = fields.Char(string="Área del proyecto")
    responsible = fields.Char(string="Responsable del proyecto")
    date_start = fields.Date(string="Fecha de inicio")
    date_finish = fields.Date(string="Fecha de caducidad")
    details = fields.Text(string="Detalles del proyecto")
    type_project = fields.Many2one('tokenizacion.type.project', string='Tipo de proyecto')
    privacy = fields.Char()
    email = fields.Char(string="Correo electrónico")
    investment_type = fields.Char(default="Proyecto")
    initial_budget = fields.Char(string="Presupuesto inicial del proyecto")
    files = fields.Binary()
    creation_date = fields.Date(string="Fecha de consulta", default=datetime.today())
    percentage = fields.Selection(selection=lambda self: self.dynamic_selection(), string="Porcentaje del proyecto para inversión colaborativa")
    faq_ids = fields.One2many('tokenizacion.faq', 'project_ids', string="FAQ")
    yields = fields.Text(string="Rendimientos")
    risks = fields.Text( string="Riesgos")
    date_ids = fields.One2many('tokenizacion.dates.projects', 'dates_ids')
    # hash_contract = fields.Char()
    hash_contract_payment = fields.Char()
    hash_contract_investment = fields.Char()
    price_token = fields.Integer(string="Precio por token")
    # per_ter = fields.Selection(selection=lambda self: self.dynamic_selection(), string="Porcentaje de rendimiento")
    # p_yield = fields.Selection(selection=lambda self: self.dynamic_selection_dates(), string="Plazo de rendimiento")
    # flag_modify = fields.Boolean(default=False)
    status = fields.Selection([
            ('public', 'Publicado'),
            ('sale', 'En venta'),
            ('final', 'Concluído'),
            ],default='public')

     #Funcion para estatus activo
    # @api.model
    def public_progressbar(self):
        self.write({
        'status': 'public',
    })
 
    #Funcion para estatus inactivo
    # @api.model
    def sale_progressbar(self):
        self.write({
        'status': 'sale',
    })
    
     #Funcion para estatus inactivo
    # @api.model
    def final_progressbar(self):
        self.write({
        'status': 'final',
    })
    
    
    def dynamic_selection(self):

      select = [('10', '10%'), ('20', '20%'), ('30', '30%'), ('40', '40%'), ('50', '50%'), ('60', '60%'), ('70', '70%'), ('80', '80%'), ('90', '90%'), ('100', '100%')]

      return select

    #email validation
    @api.onchange('email')
    def validate_mail(self):
       if self.email:
        match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', self.email)
        if match == None:
            raise ValidationError('No es un e-mail válido')

    # Create in projects models!!!!
    @api.model
    def create(self, vals):
        
        print ("entro a la sobrecarga create de properties")
        print("Valores create", vals)
        
        # -------------------------------------------------------------
        # Testing V3 -----------------------------------------------------------------------
        w3 = Web3(HTTPProvider('https://ropsten.infura.io/v3/f9736385b2d340dd976ab1df6ff51ad6'))
        # Environment V3 -----------------------------------------------------------------------
        #w3 = Web3(HTTPProvider('https://mainnet.infura.io/v3/f9736385b2d340dd976ab1df6ff51ad6'))
        # --------------------------------step 2
        # if len(sys.argv) <= 1:
        #testing
        path = "/home/dpat/TokenInvestment.json"
        #environment
        #path = "/opt/odoo13/odoo-custom-addons/TokenInvestment.json"
        # save1 = super(Properties, self).create(vals)
        _name = vals['name']
        _percentage = int(vals['percentage'])
        records = self.env['tokenizacion.projects'].search([])
        last = records and max(records)
        last_id = last.id
        print("A ver los records", last_id)
        
        if last_id == False:
            _ids = 0
        else:
            _ids = self.env['tokenizacion.projects'].search([])[-1].id
        # _ids = self.env['tokenizacion.properties'].search([])[-1].id
        # _ids = self.env['tokenizacion.properties'].search([]).id
        # print("es _ids1",_ids1)
        # print("es _ids correct",_ids)
        # if _ids == False:
        #     _ids = 0
        # else:
        #     _ids = _ids
        print("es _ids",_ids)
        # print("es _ids0",_ids0)
        # print("es _ids1",_ids1)
        _id = _ids + 1 
        print ("Se supone que es el id",_id)
        def get_symbol(self,_name,_id):
            words = _name.split(' ') 
            symbol = ''
            for word in words:
                symbol += word[0]
            return symbol + str(_id)
        _symbol = get_symbol(self,_name,_id)
        print("qué symbol es", _symbol)
        print(f"USAGE: python3 {sys.argv[0]} <TokenInvestment.json>") 
        try:
            with open(path) as inFile:
                solcOutput = json.load(inFile)
        except Exception as e:
            print(f"ERROR: Could not load file {path}: {e}")
        senderKey = "32ED1791B32C9FEFE2DD3339276087A0AE6EE009E60D9C143591A445577A09F6"
        senderAccount = w3.eth.account.privateKeyToAccount(senderKey)
        contractData = solcOutput
        contractInvestment = w3.eth.contract(abi=contractData['abi'], bytecode=contractData['bin'])
        transactionInvestment = contractInvestment.constructor(_name, _symbol, _percentage).buildTransaction({
        'gasPrice': w3.toWei('31', 'gwei'),
        'from': senderAccount.address,
        'nonce': w3.eth.getTransactionCount(senderAccount.address)})
        # Sign the transaction
        signedInvestment = w3.eth.account.signTransaction(transactionInvestment, senderAccount.privateKey)
        # Send it!
        txHashInvestment = w3.eth.sendRawTransaction(signedInvestment.rawTransaction)
        print(f"Contract deployed for Investment; Waiting to transaction receipt")
        # Wait for the transaction to be mined, and get the transaction receipt
        txReceiptInvestment = w3.eth.waitForTransactionReceipt(txHashInvestment)
        print(f"Contract Investment deployed to: {txReceiptInvestment.contractAddress}")
        contractHashInvestment = txReceiptInvestment.contractAddress
        vals['hash_contract_investment'] = contractHashInvestment
        # DEPLOY FOR PAYMENT
        contractPayment = w3.eth.contract(abi=contractData['abi2'], bytecode=contractData['bin2'])
        transactionPayment = contractPayment.constructor(contractHashInvestment).buildTransaction({
        'gasPrice': w3.toWei('21', 'gwei'),
        'from': senderAccount.address,
        'nonce': w3.eth.getTransactionCount(senderAccount.address)})
        # Sign the transaction
        signedPayment = w3.eth.account.signTransaction(transactionPayment, senderAccount.privateKey)
        # Send it!
        txHashPayment = w3.eth.sendRawTransaction(signedPayment.rawTransaction)
        print(f"Contract deployed for Payment; Waiting to transaction receipt")
        # Wait for the transaction to be mined, and get the transaction receipt
        txReceiptPayment = w3.eth.waitForTransactionReceipt(txHashPayment)
        print(f"Contract Payment deployed to: {txReceiptPayment.contractAddress}")
        contractHashPayment = txReceiptPayment.contractAddress
        vals['hash_contract_payment'] = contractHashPayment
            
        res = super(Projects, self).create(vals)


        return res
    
    # Write in projects models!!!!
    #@api.model write
    def write(self,vals):

        print ("entro a la sobrecarga write de projects")
        print ("Valores write: ",vals)
        


        # price = vals.get('price_token')
        # print("Precio token vals", price)
        # priceself = self.price_token
        # print("Precio token self", priceself)
        # if vals.get('price_token') != None:
        #     # Testing V3 -----------------------------------------------------------------------
        #     w3 = Web3(HTTPProvider('https://ropsten.infura.io/v3/f9736385b2d340dd976ab1df6ff51ad6'))
        #     # Environment V3 -----------------------------------------------------------------------
        #     #w3 = Web3(HTTPProvider('https://mainnet.infura.io/v3/f9736385b2d340dd976ab1df6ff51ad6'))
        #     _hash_contract = self.hash_contract
        #     print ("se supone que es el hash del contract",_hash_contract)
        #     token = w3.eth.contract(
        #     address = _hash_contract,
        #     abi = '[{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"spender","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":true,"internalType":"address","name":"to","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"constant":false,"inputs":[{"internalType":"string","name":"name","type":"string"},{"internalType":"string","name":"symbol","type":"string"},{"internalType":"uint8","name":"decimals","type":"uint8"},{"internalType":"uint256","name":"totalSupply","type":"uint256"},{"internalType":"uint256","name":"pricePerToken","type":"uint256"}],"name":"CreateTokenERC20","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"_owner","type":"address"},{"internalType":"address","name":"_spender","type":"address"}],"name":"allowance","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_spender","type":"address"},{"internalType":"uint256","name":"_value","type":"uint256"}],"name":"approve","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_spender","type":"address"},{"internalType":"uint256","name":"_value","type":"uint256"}],"name":"approveByOwner","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"internalType":"address","name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"balance","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"uint256","name":"_quantity","type":"uint256"}],"name":"buy","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"account","type":"address"},{"internalType":"uint256","name":"_quantity","type":"uint256"}],"name":"buyFor","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[{"internalType":"uint256","name":"pricePerToken","type":"uint256"}],"name":"changePricePerToken","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"internalType":"uint8","name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_spender","type":"address"},{"internalType":"uint256","name":"_subtractedValue","type":"uint256"}],"name":"decreaseApproval","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"internalType":"uint256","name":"quantity","type":"uint256"}],"name":"getAmountToPayed","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_spender","type":"address"},{"internalType":"uint256","name":"_addedValue","type":"uint256"}],"name":"increaseApproval","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_to","type":"address"},{"internalType":"uint256","name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"internalType":"address","name":"_from","type":"address"},{"internalType":"address","name":"_to","type":"address"},{"internalType":"uint256","name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"internalType":"bool","name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"}]'
        #     )
        #     transaction = token.functions.changePricePerToken(
        #     priceself ).buildTransaction({
        #     'gasPrice': w3.toWei('10', 'gwei'),
        #     #'gasPrice': w3.toWei(w3.eth.generateGasPrice()),
        #     'from': "0x7fB636cc460B8C7e4Aa3e6A22fE166EF5ec34eBB",
        #     'nonce': w3.eth.getTransactionCount("0x7fB636cc460B8C7e4Aa3e6A22fE166EF5ec34eBB")
        #     })
        #     private_key = "32ED1791B32C9FEFE2DD3339276087A0AE6EE009E60D9C143591A445577A09F6" 
        #     signed_txn = w3.eth.account.signTransaction(transaction, private_key=private_key)
        #     print("VER QUE HAY AQUI",signed_txn)
        #     finalwrite = w3.eth.sendRawTransaction(signed_txn.rawTransaction)
        #     final2write = finalwrite.hex()
        #     print("VER TAMBIEN QUE HAY AQUI write", final2write)
        
        # else: 
        #     print("Es igual el precio del token, así que nada")
        # per_ter_database = self.per_ter
        # print("flag in databse", per_ter_database)
        # per_ter_vals = vals.get('per_ter')
        # print("flag in vals", per_ter_vals)
        # p_yield_database = self.p_yield
        # print("flag in databse", p_yield_database)
        # p_yield_vals = vals.get('p_yield')
        # print("flag in vals", p_yield_vals)
        # vals['flag_modify'] = True
        # if self.per_ter != vals.get('per_ter') and vals.get('per_ter') != None:
        #     vals['flag_modify'] = False
        
        # elif self.p_yield != vals.get('p_yield') and vals.get('p_yield') != None:
        #     vals['flag_modify'] = False

        
        res = super(Projects, self).write(vals)
        print(res)
        
        return res


        # def modify_total_value_projects(self):
        # recordsall = self.env['tokenizacion.projects'].search([])
        # for x in recordsall:
        #     l = x.id
        #     print(l)
        #     recordset = self.env['tokenizacion.projects'].search([('id', '=', l)])
        #     print("a ver si trae los ids normales", recordset)
        #     dateNow = datetime.now()
        #     dateUpdate = recordset.write_date
        #     days = int( recordset.p_yield)
        #     percentage = int(recordset.per_ter)/100
        #     print("date ahora",dateNow)
        #     print("date inserted actual registered", dateUpdate)
        #     print("days", days)

        #     # dateInit = dateNow + timedelta(seconds=0)
        #     # print("InitDate",dateInit)
        #     # dateFinish = dateNow - timedelta(seconds=60)
        #     # print("FinishDate",dateFinish)

        #     # recordDatesModif = self.env['tokenizacion.properties'].search([('write_date', '<', dateInit), ('write_date', '>', dateFinish)])
        #     # print("records modified between dates",recordDate)

        #     print("Si FALSE entra aql ciclo",recordset.flag_modify)
            

        #     if recordset.flag_modify == False:
        #         print("La fecha seleccionada aún no ha llegado así que queda pendiente cambiarse :).")

        #         if dateUpdate == None:
        #             resDate = dateNow + timedelta(seconds=days)
        #             print("resDate", resDate)
        #         else:
        #             resDate = dateUpdate + timedelta(seconds=days)
        #             print("resDate", resDate)
                
        #         if dateNow >= resDate:
        #             print("La fecha está igual o mayor y se modificará el precio")
        #             # record_ids = self.env['tokenizacion.properties'].search([('name', '=', self.name)])
        #             # print("record_ids",record_ids)
        #             # for record in record_ids:
        #             res = recordset.total_property
        #             print("res",res)
        #             toAdd = int(res) * percentage
        #             print("toAdd",toAdd)
        #             result = float(res) + toAdd
        #             print("result",result)
        #             finalRes = str(int(result))
        #             print("finalresult", finalRes)
        #             vals1 = {
        #             'total_property': finalRes,
        #             'flag_modify': True
        #             }
        #             recordset.write(vals1)
        #             print("cambio variable flag modify",recordset.flag_modify)

        #         else:
        #             print("La fecha aún no llega todavía a la seleccionada.")
            
        #     else:
        #         print("La fecha ya ha sido seleccionada por lo que el monto ya ha sido cambiado :).")
        
        # return True




class Users(models.Model):
    _inherit = 'res.users'

    wallet = fields.Char()
    privateKey = fields.Char()
    pass_compare = fields.Char()
    names = fields.Char(string="Nombre(s)")
    lastnames = fields.Char(string="Apellido(s)")
    receipt = fields.Binary(string="Comprobante de domicilio")
    ine = fields.Binary(string="INE")
    is_root = fields.Boolean("Root")
    is_inmuebles = fields.Boolean("Inmuebles")
    is_art = fields.Boolean("Arte")
    is_projects = fields.Boolean("Proyectos")
    is_cert = fields.Boolean("Certificador")
    is_cert_student = fields.Boolean("Certificado alumnos")
    is_cert_master = fields.Boolean("Certificado administrador")
    is_license = fields.Boolean("Usuario licencia")
    deposits_ids = fields.One2many('tokenizacion.deposits', 'user_id', string="Depósitos")


    @api.model
    def create(self, vals):
        
        print ("entro a la sobrecarga create")
        print("Valores create", vals)
        
        # certSelected = vals['in_group_61']
        flagUsers = True
        # print("según es el grupo del vals", certSelected)
        model_data = self.buscar_res_data_grupos()
        user_root = model_data[0]
        user_inmuebles = model_data[1]
        user_art = model_data[2]
        user_projects = model_data[3]
        user_cert = model_data[4]
        user_cert_student = model_data[5]
        user_cert_master = model_data[6]
        user_license = model_data[7]
        print(user_cert)
        construirRoot = "in_group_" + str(user_root)
        construirInmuebles = "in_group_" + str(user_inmuebles)
        construirArt = "in_group_" + str(user_art)
        construirProjects = "in_group_" + str(user_projects)
        construirCert = "in_group_" + str(user_cert)
        construirCertStudent = "in_group_" + str(user_cert_student)
        construirCertMaster = "in_group_" + str(user_cert_master)
        construirLicense = "in_group_" + str(user_license)
        print("variable construidaCert", construirCert)
        construidaCert = construirCert
        print(construidaCert)
        varNew = vals[construidaCert]
        print("Var nueva VarNew", varNew)
        
        # if varNew == flagUsers
        #     print("Grupo certificado está seleccionado")
        # else:
        #     print("El grupo certificado no está seleccionado")
        if self.env.user.has_group('tokenizacion.group_user_root'):
            print("Es el root el que crea")
            checarVar = self.env.user.login
            print("Self.env.user  VER",checarVar)
            print("varNew",varNew,"flagUsers",flagUsers)
            if varNew == flagUsers:
                print("Grupo certificado está seleccionado")
                vals['is_cert'] = True
                
                walletData = self.create_wallet()
                print ("Se supone que me trae 2 valores wallet y private", walletData)
                walletPublic = walletData[0]
                print ("wallet", walletPublic)
                walletPrivate = walletData[1]
                print ("private", walletPrivate)
                vals['wallet'] = walletPublic
                vals['privateKey'] = walletPrivate

                res = super(Users, self).create(vals)    

                return res

            else:
                if vals[construirRoot] == flagUsers:
                    
                    vals['is_root'] = True
                    vals['is_cert'] = False
                    
                    walletData = self.create_wallet()
                    print ("Se supone que me trae 2 valores wallet y private", walletData)
                    walletPublic = walletData[0]
                    print ("wallet", walletPublic)
                    walletPrivate = walletData[1]
                    print ("private", walletPrivate)
                    vals['wallet'] = walletPublic
                    vals['privateKey'] = walletPrivate

                    res = super(Users, self).create(vals)

                    return res
                
                elif vals[construirInmuebles] == flagUsers:
                    
                    vals['is_inmuebles'] = True
                    vals['is_cert'] = False
                    print("El grupo certificado no está seleccionado así que hay wallet")
                    
                    walletData = self.create_wallet()
                    print ("Se supone que me trae 2 valores wallet y private", walletData)
                    walletPublic = walletData[0]
                    print ("wallet", walletPublic)
                    walletPrivate = walletData[1]
                    print ("private", walletPrivate)
                    vals['wallet'] = walletPublic
                    vals['privateKey'] = walletPrivate
                    
                    res = super(Users, self).create(vals)

                    return res
                
                elif vals[construirArt] == flagUsers:
                    
                    vals['is_art'] = True
                    vals['is_cert'] = False
                    print("El grupo certificado no está seleccionado así que hay wallet")
                    
                    walletData = self.create_wallet()
                    print ("Se supone que me trae 2 valores wallet y private", walletData)
                    walletPublic = walletData[0]
                    print ("wallet", walletPublic)
                    walletPrivate = walletData[1]
                    print ("private", walletPrivate)
                    vals['wallet'] = walletPublic
                    vals['privateKey'] = walletPrivate
                    
                    res = super(Users, self).create(vals)

                    return res

                elif vals[construirProjects] == flagUsers:
                    
                    vals['is_projects'] = True
                    vals['is_cert'] = False
                    print("El grupo certificado no está seleccionado así que hay wallet")
                    
                    walletData = self.create_wallet()
                    print ("Se supone que me trae 2 valores wallet y private", walletData)
                    walletPublic = walletData[0]
                    print ("wallet", walletPublic)
                    walletPrivate = walletData[1]
                    print ("private", walletPrivate)
                    vals['wallet'] = walletPublic
                    vals['privateKey'] = walletPrivate

                    res = super(Users, self).create(vals)

                    return res
                
                elif vals[construirCertStudent] == flagUsers:
                    
                    vals['is_cert_student'] = True
                    vals['is_cert'] = False
                    print("El grupo certificado no está seleccionado así que hay wallet")
                    
                    walletData = self.create_wallet()
                    print ("Se supone que me trae 2 valores wallet y private", walletData)
                    walletPublic = walletData[0]
                    print ("wallet", walletPublic)
                    walletPrivate = walletData[1]
                    print ("private", walletPrivate)
                    vals['wallet'] = walletPublic
                    vals['privateKey'] = walletPrivate

                    res = super(Users, self).create(vals)

                    return res
                
                elif vals[construirCertMaster] == flagUsers:
                    
                    vals['is_cert_master'] = True
                    vals['is_cert'] = False
                    print("El grupo certificado no está seleccionado así que hay wallet")
                    
                    walletData = self.create_wallet()
                    print ("Se supone que me trae 2 valores wallet y private", walletData)
                    walletPublic = walletData[0]
                    print ("wallet", walletPublic)
                    walletPrivate = walletData[1]
                    print ("private", walletPrivate)
                    vals['wallet'] = walletPublic
                    vals['privateKey'] = walletPrivate
                    
                    res = super(Users, self).create(vals)

                    return res
                
                elif vals[construirLicense] == flagUsers:
                    
                    vals['is_license'] = True
                    vals['is_cert'] = False
                    print("El grupo certificado no está seleccionado así que hay wallet")
                    
                    walletData = self.create_wallet()
                    print ("Se supone que me trae 2 valores wallet y private", walletData)
                    walletPublic = walletData[0]
                    print ("wallet", walletPublic)
                    walletPrivate = walletData[1]
                    print ("private", walletPrivate)
                    vals['wallet'] = walletPublic
                    vals['privateKey'] = walletPrivate
                    
                    res = super(Users, self).create(vals)

                    return res
    

        elif self.env.user.has_group('tokenizacion.group_user_certs'):
            print("Es el usuario certificador el que crea así que no hay wallet")
            vals['is_cert_student'] = True
            
            walletData = self.create_wallet()
            print ("Se supone que me trae 2 valores wallet y private", walletData)
            walletPublic = walletData[0]
            print ("wallet", walletPublic)
            walletPrivate = walletData[1]
            print ("private", walletPrivate)
            vals['wallet'] = walletPublic
            vals['privateKey'] = walletPrivate

            res = super(Users, self).create(vals)

            return res
    
        # if vals.get('in_group_61') == False:
        #     print("Me está trayendo los vals")
        # result = []
		# try:
		# 	dummy,group_id = dataobj.get_object_reference('base', 'group_user')
		# 	result.append(group_id)
        #     print(result.append(group_id))s
		# 	dummy,group_id = dataobj.get_object_reference('base', 'group_partner_manager')
		# 	result.append(group_id)
        #     print(result.append(group_id))
        # except ValueError:
		# 	# If these groups does not exists anymore
		# 	print ("ocurrio un error")
		# 	pass
		# print ("este es el result",result)
    
    def create_wallet(self):
        
        w3 = Web3(HTTPProvider('https://ropsten.infura.io/v3/f9736385b2d340dd976ab1df6ff51ad6'))
        # Environment V3 -----------------------------------------------------------------------
        #w3 = Web3(HTTPProvider('https://mainnet.infura.io/v3/f9736385b2d340dd976ab1df6ff51ad6'))
        account = w3.eth.account.create('KEYSMASH FJAFJKLDSKF7JKFDJ 1530')
        address = account.address
        privateKey = account.privateKey
        privateKeyWell = account.privateKey.hex()
        encoded = privateKeyWell.encode('utf-8').strip()
        # vals['wallet'] = address
        # vals['privateKey'] = encoded
        # _balance = blockcypher.get_total_balance(vals['name'])
        # vals['balance'] = _balanc
        # print("Resaultado a ver lo que sea", account)
        # print("Resaultado a ver address de wallet", address)
        # print("Resaultado a ver llave privada", encoded)
        # print("Resaultado a ver llave privada", privateKey)
        # print("Resaultado a ver llave privada", privateKeyWell)

        return address,encoded


    def buscar_res_data_grupos(self):

        root = self.env['ir.model.data'].search([('model', '=','res.groups'),('module','=','tokenizacion'),('name','=','group_user_root')])
        print("Res root: ",root.res_id)
        
        inmuebles = self.env['ir.model.data'].search([('model', '=','res.groups'),('module','=','tokenizacion'),('name','=','group_user_inmuebles')])
        print("Res inmuebles: ",inmuebles.res_id)
        
        art = self.env['ir.model.data'].search([('model', '=','res.groups'),('module','=','tokenizacion'),('name','=','group_user_art')])
        print("Res art: ",art.res_id)
        
        projects = self.env['ir.model.data'].search([('model', '=','res.groups'),('module','=','tokenizacion'),('name','=','group_user_projects')])
        print("Res projects: ",projects.res_id)
        
        certs = self.env['ir.model.data'].search([('model', '=','res.groups'),('module','=','tokenizacion'),('name','=','group_user_certs')])
        print("Res certs: ",certs.res_id)
        
        certs_student = self.env['ir.model.data'].search([('model', '=','res.groups'),('module','=','tokenizacion'),('name','=','group_user_certs_student')])
        print("Res certs: ",certs.res_id)
        
        certs_master = self.env['ir.model.data'].search([('model', '=','res.groups'),('module','=','tokenizacion'),('name','=','group_user_certs_master')])
        print("Res certs: ",certs_master.res_id)
        
        license_user = self.env['ir.model.data'].search([('model', '=','res.groups'),('module','=','tokenizacion'),('name','=','group_user_license')])
        print("Res license: ",license_user.res_id)

        lista_res = [root.res_id, inmuebles.res_id, art.res_id, projects.res_id, certs.res_id, certs_student.res_id, certs_master.res_id, license_user.res_id]

        return lista_res



class TypeProperty(models.Model):
  _name = 'tokenizacion.type.property'

  name = fields.Char(string="Tipo de inmueble")


class TypeArt(models.Model):
  _name = 'tokenizacion.type.art'

  name = fields.Char(string="Tipo de obra de arte")


class TypeProject(models.Model):
  _name = 'tokenizacion.type.project'

  name = fields.Char(string="Tipo de proyecto")


class FAQ(models.Model):
  _name = 'tokenizacion.faq'

  name = fields.Char(string="FAQ")
  answer = fields.Text(string="Respuesta")
  inmuebles_ids = fields.Many2one('tokenizacion.properties', string="Tipo de inmueble")
  art_ids = fields.Many2one('tokenizacion.art', string="Tipo de obra de arte")
  project_ids = fields.Many2one('tokenizacion.projects', string="Tipo de proyecto")

class DatesProjects(models.Model):
    _name = 'tokenizacion.dates.projects'

    name = fields.Date()
    dates_ids = fields.Many2one('tokenizacion.projects')


class PushTokensProperties(models.Model):
    _name = 'tokenizacion.push.tokens.properties'

    name = fields.Many2one('res.users', string="usuario")
    quantity = fields.Integer(string="Cantidad de tokens comprados")
    payment_currency = fields.Char(string="Metodo de pago")
    hash_id =  fields.Char(string="hash de compra")
    id_inversion = fields.Many2one('tokenizacion.properties', "Inversiones")
    creation_date = fields.Date(string="Fecha de compra")



class PushTokensArt(models.Model):
    _name = 'tokenizacion.push.tokens.art'

    name = fields.Many2one('res.users', string="usuario")
    quantity = fields.Integer(string="Cantidad de tokens comprados")
    payment_currency = fields.Char(string="Metodo de pago")
    hash_id =  fields.Char(string="hash de compra")
    id_inversion = fields.Many2one('tokenizacion.art', "Inversiones")
    creation_date = fields.Date(string="Fecha de compra")


class PushTokensProjects(models.Model):
    _name = 'tokenizacion.push.tokens.projects'

    name = fields.Many2one('res.users', string="usuario")
    quantity = fields.Integer(string="Cantidad de tokens comprados")
    payment_currency = fields.Char(string="Metodo de pago")
    hash_id =  fields.Char(string="hash de compra")
    id_inversion = fields.Many2one('tokenizacion.projects', "Inversiones")
    creation_date = fields.Date(string="Fecha de compra")

class BankDeposit(models.Model):
    _name = 'tokenizacion.deposits'

    name = fields.Char(string="Clave de seguimiento")
    reference = fields.Char(string="Referencia bancaria")
    bank = fields.Char(string="Banco")
    quantity = fields.Char(string="Cantidad")
    creation_date = fields.Date(string="Fecha del depósito", default=datetime.today())
    user_id = fields.Many2one('tokenizacion.users', "Depósitos")

class Certs(models.Model):
    _name = 'tokenizacion.certs'

    def _get_key(self):
        longitud = 60
        valores = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        cryptogen = SystemRandom()
        key = ""
        while longitud > 0:
            key = key + cryptogen.choice(valores)
            longitud = longitud - 1
        print(key)
        
        return key


    name = fields.Char(string="Nombre del certificado")
    institution = fields.Char(string="Institución")
    inCharge = fields.Many2one('res.users', string="Encargado")
    quantity = fields.Integer(string="Cantidad de certificados máximos")
    key = fields.Char(compute='_get_key', default=_get_key, string="Clave de certificado", store =True)
    user = fields.Many2one('res.users', string="Usuario certificado")
    user3 = fields.Many2many('res.users', string="Usuario certificado", select=True, store=True, track_visibility='onchange')
    #button_pdf = fields.Binary(compute='_make_certificate', default=_make_certificate, string="Descargar certificado", store =True)
    test_bin = fields.Binary(string="Certificado")
    creation_date = fields.Date(string="Fecha de registro", default=datetime.today())



    @api.model
    def create(self, vals):
        
        print ("entro a la sobrecarga create")
        print("Valores create", vals)

        # seeKey = _get_key(self)
        # print ("A ver si va bien el key", seeKey)
        res = super(Certs, self).create(vals)
        varRes = self.env.user.id
        # newCert = self.uid
        print (varRes)
        # print (newCert)
        # varNew = self.read([user])
        # varNew2 = self.search([]).user
        # i = []
        # for i in varNew2:
        #     if i == varRes:
        #         valNew = i
        #         print("Val New a ver qué pedos.",valNew)
        # # print("a ver si es el id del cert", varNew)
        # print("a ver si es el id del cert", varNew2)
        # if varRes == varNew2:
        #     print ("Es igual esta ")
        # else:
        #     print ("Parece que siempre no")

        
        return res


    def write(self,vals):

        print ("entro a la sobrecarga write de inmuebles")
        print ("Valores write: ",vals)
        
        
        dataName = self.name
        print("dataName",dataName)

        res = super(Certs, self).write(vals)
        print(res)  
        
        return res


class License(models.Model):
    _name = 'tokenizacion.license'

    name = fields.Char(string="Nombre de la licencia")
    quantityToSell = fields.Integer(string="Cantidad de licencias a vender")
    quantityToUse = fields.Integer(string="Número de veces disponible de uso")
    price = fields.Char(string="Precio por licencia")
    expireTime = fields.Date(string="Fecha de expiración de la licencia")
    startTime = fields.Date(string="Fecha de inicio en vigencia de la licencia")
    imageLicense = fields.Binary(string="Imagen de la licencia")
    creation_date = fields.Date(string="Fecha de registro", default=datetime.today())
    telephone = fields.Char()
    email = fields.Char(string="Correo electrónico")

    @api.onchange('quantityToSell','quantityToUse')
    def validate_quantity(self):
        if self.quantityToSell < 0:
            raise ValidationError('La cantidad de licencias a vender tiene que ser un valor positivo y arriba de "0"')
        if self.quantityToUse < 0:
            raise ValidationError('El número de veces disponible en uso tiene que ser un valor positivo y arriba de "0"')

class PushTokensLicense(models.Model):
    _name = 'tokenizacion.push.tokens.license'

    name = fields.Many2one('res.users', string="usuario")
    quantity = fields.Integer(string="Cantidad de tokens comprados")
    payment_currency = fields.Char(string="Metodo de pago")
    hash_id =  fields.Char(string="hash de compra")
    id_license = fields.Many2one('tokenizacion.license', "Licencia")
    creation_date = fields.Date(string="Fecha de compra")
    
class PushTokensDefaultCurrency(models.Model):
    _name = 'tokenizacion.push.tokens.default.currency'

    name = fields.Many2one('res.users', string="usuario")
    currency = fields.Text(string="currency")
    creation_date = fields.Date(string="Fecha de compra")

# class Maps(models.Model):
#   _inherit = 'res.partner'

#   google_map_partner = fields.Char(string="Map")

