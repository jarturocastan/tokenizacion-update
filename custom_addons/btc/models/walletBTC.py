from odoo import models, fields, api
from blockchain import blockexplorer
import blockcypher
from datetime import datetime


class Wallet(models.Model):
    _name = 'btc.wallet'

    name = fields.Char()
    balance = fields.Float(digits=(10, 18), string="balance")
    date = fields.Date(string="Fecha de consulta", default=datetime.today())

    @api.model
    def create(self, vals):
        
        print ("entro a la sobrecarga create")
        print("Valores create", vals)
        _balance = blockcypher.get_total_balance(vals['name'])
        vals['balance'] = _balance

        print("Resaultado a ver", _balance)
        res = super(Wallet, self).create(vals)

        return res